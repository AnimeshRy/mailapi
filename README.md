# Mail API

To Run this on you local machine, follow these steps

```
git clone https://gitlab.com/AnimeshRy/mailapi.git
pip3 install -r requirements.txt
cd mail
python manage.py makemigrations
python manage.py migrate
python manage.py runserver
```
To create superuser
```
python manage.py createsuperuser
```
